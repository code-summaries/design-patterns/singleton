from typing import Callable

from singleton.lib.levels import LogLevel


class Logger:
    """Class variant of Singleton."""

    _level: LogLevel = LogLevel.WARNING
    _presenter: Callable[[str], str | None] = print

    @classmethod
    def setup(cls, level: LogLevel, presenter: Callable[[str], str | None]) -> None:
        cls._level = level
        cls._presenter = presenter

    @classmethod
    def debug(cls, message: str) -> str | None:
        return cls._present_message(LogLevel.DEBUG, message)

    @classmethod
    def info(cls, message: str) -> str | None:
        return cls._present_message(LogLevel.INFO, message)

    @classmethod
    def warning(cls, message: str) -> str | None:
        return cls._present_message(LogLevel.WARNING, message)

    @classmethod
    def _present_message(cls, message_level: LogLevel, message: str) -> str | None:
        if cls._level.value >= message_level.value:
            return cls._presenter(f"{message_level.name}: {message}")
        return None
