from __future__ import annotations

from typing import Any, Callable

from singleton.lib.levels import LogLevel


class Singleton(type):
    _instances: dict[type, Singleton] = {}

    def __call__(cls, *args: Any, **kwargs: dict[Any, Any]) -> Singleton:
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class Logger(metaclass=Singleton):
    """Instance variant of Singleton."""

    def __init__(
        self, level: LogLevel = LogLevel.WARNING, presenter: Callable[[str], str | None] = print
    ):
        self._level = level
        self._presenter = presenter

    def set_level(self, level: LogLevel) -> None:
        self._level = level

    def debug(self, message: str) -> str | None:
        return self._present_message(LogLevel.DEBUG, message)

    def info(self, message: str) -> str | None:
        return self._present_message(LogLevel.INFO, message)

    def warning(self, message: str) -> str | None:
        return self._present_message(LogLevel.WARNING, message)

    def _present_message(self, message_level: LogLevel, message: str) -> str | None:
        if self._level.value >= message_level.value:
            return self._presenter(f"{message_level.name}: {message}")
        return None
