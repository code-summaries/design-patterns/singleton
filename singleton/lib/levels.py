import enum


class LogLevel(enum.Enum):
    WARNING = 1
    INFO = 2
    DEBUG = 3
