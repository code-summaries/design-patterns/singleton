def user_input_prompt() -> str:
    prompt_text = (
        "Welcome to the domotics api installer. \n"
        "Please type the digit of the desired logging level and press enter: \n"
        "    1. Only warnings \n"
        "    2. Warnings and info messages \n"
        "    3. All messages \n"
    )
    return input(prompt_text)
