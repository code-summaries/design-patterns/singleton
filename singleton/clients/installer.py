from typing import Callable

from singleton.lib.levels import LogLevel
from singleton.lib.prompts import user_input_prompt
from singleton.pattern.class_logger import Logger


def run_install(
    select_log_level: Callable = user_input_prompt, log_presenter: Callable = print
) -> None:
    setup_logger(select_log_level, log_presenter)

    connect_to_repository()
    install_dependencies()
    open_port()

    Logger.info("Install finished successfully.")


def setup_logger(select_log_level: Callable, log_presenter: Callable) -> None:
    selected_log_level = select_log_level()
    if selected_log_level not in ["1", "2", "3"]:
        log_presenter("Selected log level is not supported. Using default level: 2.")
        selected_log_level = "2"

    log_level = LogLevel(int(selected_log_level))
    Logger.setup(level=log_level, presenter=log_presenter)


def connect_to_repository() -> None:
    Logger.info("Attempting to connect to repository")
    Logger.debug("Connect to domotics.com/repo1")
    Logger.warning("Connection timed out, retrying...")
    Logger.debug("Connect to domotics.com/repo2")
    Logger.info("Established connection.")
    Logger.info("Downloading dependencies")


def install_dependencies() -> None:
    Logger.debug("Found downloaded dependency: '/power-net-connect'")
    Logger.info("Installed power-net-connect")
    Logger.debug("Found downloaded dependency: '/switch-control'")
    Logger.info("Installed switch-control")


def open_port() -> None:
    Logger.debug("Opening port 8000.")
    Logger.info("Port opened successfully.")
