[tool.poetry]
name = "singleton"
version = "1.0.0"
description = "A python example of the singleton design pattern."
authors = ["Bram Vermaas <amavermaas@gmail.com>"]

[tool.poetry.dependencies]
python = "^3.11"

[tool.poetry.dev-dependencies]
flake8 = "^6.1.0"
black = "^23.7.0"
isort = "^5.12.0"
mypy = "^1.5.1"
pylint = "^2.17.5"
flake8-black = "^0.3.6"
flake8-bugbear = "^23.7.10"
flake8-builtins = "^2.1.0"
flake8-fixme = "^1.1.1"
flake8-isort = "^6.0.0"
Flake8-pyproject = "^1.2.3"
coverage = "^7.3.0"

[tool.poetry.scripts]
singleton = "singleton.__main__:main"
tests = "tests.run:all_tests"
coverage_report = "tests.run:coverage_report"
coverage_html = "tests.run:coverage_html"

[tool.isort]
multi_line_output = 3
use_parentheses = true
profile = "black"
src_paths = ["singleton"]

[tool.mypy]
files = "singleton"
python_version = "3.11"
warn_redundant_casts = true
ignore_missing_imports = true
incremental = true
disallow_untyped_defs = true

[tool.flake8]
max-line-length = 100
ignore = [
    "W503", # line break before binary operator
    "ANN101", # Missing type annotation for self in method
    "ANN102", # Missing type annotation for cls in classmethod
]

[tool.black]
line-length = 100

[tool.pylint]
disable = [
    "missing-module-docstring",
    "missing-class-docstring",
    "missing-function-docstring",
    "fixme",
    "too-few-public-methods",
    "too-many-arguments",
    "super-init-not-called",
    "duplicate-code",
]
max-attributes = 10
max-line-length = 100

[tool.coverage.run]
source = ["singleton"]
omit = ["**/__init__.py"]

[tool.coverage.report]
exclude_lines = ["raise NotImplementedError"]

[tool.coverage.html]
directory = "coverage_html"

[build-system]
requires = ["poetry-core"]
build-backend = "poetry.core.masonry.api"
