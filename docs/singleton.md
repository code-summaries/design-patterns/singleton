<div align="center">
  <h1>Singleton</h1>
</div>

<div align="center">
  <img src="singleton_icon.png" alt="drawing" width="250"/>
</div>

## Table of Contents

1. **[What is it?](#what-is-it)**
    1. [Real-World Analogy](#real-world-analogy)
    2. [Participants](#participants)
    3. [Collaborations](#collaborations)
2. **[When do you use it?](#when-do-you-use-it)**
    1. [Motivation](#motivation)
    2. [Known Uses](#known-uses)
    3. [Categorization](#categorization)
    4. [Aspects that can vary](#aspects-that-can-vary)
    5. [Solution to causes of redesign](#solution-to-causes-of-redesign)
    6. [Consequences](#consequences)
    7. [Relations with Other Patterns](#relations-with-other-patterns)
3. **[How do you apply it?](#how-do-you-apply-it)**
    1. [Structure](#structure)
    2. [Variations](#variations)
    3. [Implementation](#implementation)
4. **[Sources](#sources)**

<br>
<br>


## What is it?

> :large_blue_diamond: **Singleton is a creational pattern which ensures a class only has one instance, and provides a global point of access to it.**

### Real-World Analogy

_The central bank._

There is only one central bank in a country (the only instance of the Singleton).
The central bank manages and controls critical resources and financial policies.
And all financial institutions and government agencies (analogy for clients), have access to the central bank's services and policies

### Participants

- :man: **Singleton**
  - Provides an implementation for:
    - A private constructor [*](#caveats)
    - `getInstance`: method to return the only instance of the class

### Collaborations

Clients access a Singleton instance solely through Singleton's Instance operation.

<br>
<br>

## When do you use it?

> :large_blue_diamond: **When there must be exactly one instance of a class, and it must be accessible to clients from a
well-known access point.**

### Motivation

- How do we control access to some shared resource?
    - for example: a database connection, configuration or logger.
- While at the same time making it easy to access anywhere in the program?

### Known Uses

- Logging
- (user) Session
- Database Connection
- Thread pool
- Configuration or Preferences (manager)
- Cache (manager)
- Device drivers (e.g: for a printer or graphics card)
- Clock (to manipulate time during tests)

### Categorization

Purpose:  **Creational**  
Scope:    **Object**   
Mechanisms: **Private constructor**, **Static creation method**

Creational design patterns abstract the instantiation process. Usually an object creational pattern will delegate
instantiation to another object. In this case the class manages the only instance of itself that can exist.

### Aspects that can vary

The sole instance of a class

### Solution to causes of redesign

None (known)

### Consequences

| Advantages                                                                                                                                                                                                                                          | Disadvantages                                                                                                                                                                                            |
|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| :heavy_check_mark: **Controlled access to sole instance** <br> Because the singleton class encapsulates its sole instance, it can have strict control over how and when clients access it.                                                          | :x: **Coupling to Concrete Implementation** <br> Couples all callers to a concrete implementation of the Singleton                                                                                       |
| :heavy_check_mark: **Reduced namespace** <br> Removes need to pollute namespace with global variables                                                                                                                                               | :x: **Dependencies are hidden** <br> Instead of being exposed through the interface by for example using dependency injection.                                                                           |
| :heavy_check_mark: **Permits refinement of operations and representations** <br> Singleton can be subclassed                                                                                                                                        | :x: **Harder to test** <br> Can't easily create a new (fresh) instance of the Singleton for each test.                                                                                                   |
| :heavy_check_mark: **Permits a variable number of instances** <br> Client doesn’t need to know it’s dealing with a single instance. If the need for multiple instances arises, only the implementation of the singleton constructor needs to change | :x: **Promotes tight coupling** <br> Because of global access, many clients might use (and become coupled to) the Singleton                                                                              |
|                                                                                                                                                                                                                                                     | :x: **Shared Mutable State** <br> Multiple clients might change the state of the singleton. Making it harder to understand why the singleton is in a particular state at any given point in the program. |
|                                                                                                                                                                                                                                                     | :x: **Harder to make multithreaded** <br> Requires special treatment in a multithreaded environment so that multiple threads won’t create a singleton object several times.                              |

### Relations with Other Patterns

_Distinction from other patterns:_

- A Flyweight resembles Singleton if all shared states of the objects are stored as one flyweight object. 
   - There is only one Singleton instance, whereas a Flyweight class can have multiple instances with different intrinsic states.
   - The Singleton object can be mutable. Flyweight objects are immutable.


_Combination with other patterns:_

- A Facade class can often be transformed into a Singleton since a single facade object is sufficient in most
  cases.
- Abstract Factories, Builders and Prototypes can all be implemented as Singletons.

<br>
<br>

## How do you apply it?

> :large_blue_diamond: **A singleton class with a static creation method that always returns the same (cached) object.**

### Structure

```mermaid
classDiagram
    class Client {
    }

    class Singleton {
        - instance: Singleton
        + getInstance(): Singleton
    }

    Singleton <-- Singleton: creates
    Client --> Singleton: uses
```

### Variations

- **Instance Singleton**
    - Private constructor [*](#caveats) and `getInstance` method ensures there is always only one (or controlled number)
      of instances.
        - :heavy_check_mark: Permits a variable number of instances.
        - :x: More complicated.
- **Class SIngleton**
    - No instances are created, only the class is used.
        - :heavy_check_mark: Simple.
        - :x: Harder to allow multiple instances of the Singleton later.

### Implementation

In this example the Singleton is used for a logger:

- [Instance variant](../singleton/pattern/instance_logger.py)
- [Class variant](../singleton/pattern/class_logger.py)

The client is a simple installation application that logs its progress:

- [Client](../singleton/clients/installer.py)

Ofcourse the tests are clients of the logger too:

- [Test instance variant](../tests/unit/test_instance_singleton.py)
- [Test class variant](../tests/unit/test_class_singleton.py)

<br>
<br>

## Sources

- [Design Patterns: Elements of Reusable Object-Oriented Software](https://www.amazon.com/Design-Patterns-Elements-Reusable-Object-Oriented/dp/0201633612)
- [Refactoring Guru - Singleton](https://refactoring.guru/design-patterns/singleton)
- [Head First Design Patterns: A Brain-Friendly Guide](https://www.amazon.com/Head-First-Design-Patterns-Brain-Friendly/dp/0596007124)
- [6 Reasons Why You Should Avoid Singleton](https://www.davidtanzer.net/david's%20blog/2016/03/14/6-reasons-why-you-should-avoid-singletons.html)
- [ArjanCodes: QUESTIONABLE Object Creation Patterns in Python](https://www.youtube.com/watch?v=Rm4JP7JfsKY&)
- [ZoranHorvat: Don’t Let Your Domain Model Depend on Current Time!](https://youtu.be/DHIDcInpkLc?si=howHd-_Z0mcLHjkK)

<br>
<br>

#### Caveats

_* Since it’s impossible to create a private constructor in python, a modified constructor is used instead._
