from dataclasses import dataclass
from typing import Callable

from tests.test_doubles.spies import LogCollector

from singleton.clients.installer import run_install

LOG_LEVEL_SELECTORS = {
    "1. Only warnings": lambda: "1",
    "2. Warnings and info messages": lambda: "2",
    "3. All messages": lambda: "3",
    "42. Non-existent level": lambda: "42",
}


@dataclass
class LogContent:
    has_warning: bool
    has_info: bool
    has_debug: bool


class Installer:
    def __init__(self):
        self._log_collector = LogCollector()
        self._log_level_selector: Callable | None = None

    def select_level(self, level: str) -> None:
        self._log_level_selector = LOG_LEVEL_SELECTORS.get(level)

    def run(self) -> None:
        run_install(
            select_log_level=self._log_level_selector,
            log_presenter=self._log_collector.collect_log,
        )

    def assert_only_warning_messages_logged(self) -> None:
        self._assert_log_content_equals(
            LogContent(
                has_warning=True,
                has_info=False,
                has_debug=False,
            )
        )

    def assert_warning_and_info_messages_logged(self) -> None:
        self._assert_log_content_equals(
            LogContent(
                has_warning=True,
                has_info=True,
                has_debug=False,
            )
        )

    def assert_all_messages_logged(self) -> None:
        self._assert_log_content_equals(
            LogContent(
                has_warning=True,
                has_info=True,
                has_debug=True,
            )
        )

    def assert_user_error_logged(self) -> None:
        expected_logs = "Selected log level is not supported."
        actual_logs = self._log_collector.get_collected_logs()
        assert (
            expected_logs in actual_logs
        ), f"Expected logs: '{expected_logs}', not found in: '{actual_logs}'"

    def _assert_log_content_equals(self, expected_log_content: LogContent) -> None:
        actual_log_content = self._get_log_content()
        assert (
            actual_log_content == expected_log_content
        ), f"Expected: {expected_log_content}, got: {actual_log_content}."

    def _get_log_content(self) -> LogContent:
        logs = self._log_collector.get_collected_logs()
        return LogContent(
            has_warning="WARNING: " in logs,
            has_info="INFO: " in logs,
            has_debug="DEBUG" in logs,
        )
