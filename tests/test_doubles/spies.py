def spy_presenter(message: str) -> str:
    return message


class LogCollector:
    def __init__(self):
        self._logs = ""

    def collect_log(self, message: str) -> None:
        if isinstance(message, str):
            self._logs += message

    def get_collected_logs(self) -> str:
        return self._logs
