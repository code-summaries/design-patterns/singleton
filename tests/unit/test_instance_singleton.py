import unittest

from tests.test_doubles.spies import spy_presenter

from singleton.lib.levels import LogLevel
from singleton.pattern.instance_logger import Logger


class TestInstanceVariant(unittest.TestCase):
    def setUp(self) -> None:
        self.logger = Logger(presenter=spy_presenter)
        self.logger.set_level(LogLevel.INFO)

    def test_log_message_with_same_level_as_logger_level(self) -> None:
        logged_message = self.logger.info("User is informed!")
        self.assertEqual("INFO: User is informed!", logged_message)

    def test_no_log_message_if_level_lower_than_logger_level(self) -> None:
        logged_message = self.logger.debug("User sees bugs.")
        self.assertIsNone(logged_message)

    def test_log_message_with_higher_than_logger_level(self) -> None:
        logged_message = self.logger.warning("User is warned!")
        self.assertEqual("WARNING: User is warned!", logged_message)

    def test_initializing_new_logger_doesnt_affect_settings(self) -> None:
        logger2 = Logger(level=LogLevel.WARNING)
        logged_message = logger2.info("Some info.")
        self.assertEqual("INFO: Some info.", logged_message)

    def test_set_level_affects_all_loggers(self) -> None:
        logger2 = Logger()
        logger2.set_level(LogLevel.WARNING)
        logged_message = self.logger.info("More info.")
        self.assertIsNone(logged_message)
