import unittest

from tests.test_doubles.spies import spy_presenter

from singleton.lib.levels import LogLevel
from singleton.pattern.class_logger import Logger


class TestClassVariant(unittest.TestCase):
    def setUp(self) -> None:
        Logger.setup(level=LogLevel.INFO, presenter=spy_presenter)

    def test_log_message_with_same_level_as_logger_level(self) -> None:
        logged_message = Logger.info("User is informed!")
        self.assertEqual("INFO: User is informed!", logged_message)

    def test_no_log_message_if_level_lower_than_logger_level(self) -> None:
        logged_message = Logger.debug("User sees bugs.")
        self.assertIsNone(logged_message)

    def test_log_message_with_higher_than_logger_level(self) -> None:
        logged_message = Logger.warning("User is warned!")
        self.assertEqual("WARNING: User is warned!", logged_message)

    def test_all_loggers_share_the_same_settings(self) -> None:
        Logger.setup(level=LogLevel.DEBUG, presenter=spy_presenter)
        logged_message = Logger.debug("Some bugs.")
        self.assertEqual("DEBUG: Some bugs.", logged_message)
