import unittest

from tests.sut_drivers.installer import Installer


class TestInstallerClient(unittest.TestCase):
    def setUp(self) -> None:
        self.installer = Installer()

    def test_select_warning_level_logs_only_warning_messages(self) -> None:
        self.installer.select_level("1. Only warnings")

        self.installer.run()

        self.installer.assert_only_warning_messages_logged()

    def test_select_info_level_logs_warning_and_info_messages(self) -> None:
        self.installer.select_level("2. Warnings and info messages")

        self.installer.run()

        self.installer.assert_warning_and_info_messages_logged()

    def test_select_all_messages_level_logs_all_messages(self) -> None:
        self.installer.select_level("3. All messages")

        self.installer.run()

        self.installer.assert_all_messages_logged()

    def test_select_invalid_level_logs_user_error(self) -> None:
        self.installer.select_level("42. Non-existent level")

        self.installer.run()

        self.installer.assert_user_error_logged()
